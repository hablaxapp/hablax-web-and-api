<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class WebRTC_Api_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

    function get_account_sip($account_id){
        $where = array('account_id' => $account_id);
        $query = $this->db->get_where('accounts_sips', $where, 1);

        if ($query->num_rows() > 0) {
            #verificar si está activo
            return  $query->row();
        } else {
            return FALSE;
        }
    }

  function get_account_data($email){
    $where = array('email' => $email);
		$query = $this->db->get_where('accounts', $where, 1);

		if ($query->num_rows() > 0) {
			#verificar si está activo
			return  $query->row();
		} else {
			return FALSE;
		}
  }

  public function insert_account_sip($account_id, $account_sip)
	{
		$data = array(
						'account_id'       => $account_id,
                        'host'  => $account_sip->host,
                        'wss'  => $account_sip->wss,
                        'username'  => $account_sip->username,
                        'password'  => $account_sip->password,
                        'uri'  => $account_sip->uri,
                        'sip_device_id'  => $account_sip->row_id
		 			);

		if ($this->db->insert('accounts_sips', $data)) {
			return TRUE;
		} else {
			return FALSE;
		}

	}
  
  

  public function create_sip_device($account_id, $u, $p)
	{
		$data = array(
						'username'       => $u,
						'sip_profile_id' => '1',
						'accountid'      => $account_id,
						'dir_params'     => json_encode(array('password' => $p)),
						'dir_vars'       => '{"effective_caller_id_name":"www.hablax.com","effective_caller_id_number":"13056005530","user_context":"default"}'
		 			);
		if ($this->db->insert('sip_devices', $data)) {
			return $this->db->insert_id();
		} else {
			return FALSE;
		}
	}

	public function get_user_data($account_id)
	{
		return $this->db->get_where('accounts', array('id' => $account_id), 1)->row();
	}

	
}