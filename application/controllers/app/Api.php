<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
class Api extends CI_Controller {

	private $token; private $user_id; private $user_data; private $result; private $version; private $encrypt_key; private $nonce; private $account; private $description; private $author; private $db_table_name; private $sip; private $funds_tax ; private $promo_cuba;

	public function __construct()
	{
		//Controlador que manejará el funcionamiento de la APPpzre
		//NO lo llevaremos a modelo, lo usaremos acá mismo.
		parent::__construct();
		$this->load->model('App_model');
		$this->load->helper('app');
		$this->load->library('email');
		$this->load->model('administration/upload_popup_model');
		$this->load->model('Recargas_model');
		$this->load->model('sms_model');
		$this->load->model('administration/options_model');
		$this->load->model('manager_model', 'manager');
		$this->load->model('tarifas_model', 'tarifas');
		$this->load->model('admin/payment_model', 'payment');

		$this->api_version          = "2.0";
		$this->app_version          = "2.2.0";
		$this->app_minimum_version  = "2.2.0";
		$this->test_minimum_version = "2.2.0";
		$this->description          = "API for Hablax.com Hybrid app";
		$this->author               = "Ing. Gian Barboza R.";
		$this->encrypt_key          = "BkT07bCHhbVQagia7wx4CkYSxcxP7jn7";
		$this->db_table_name        = "app_sessions";
		$this->funds_tax            = "0.07";
		$this->result               = new stdClass();
		$this->sandbox              = FALSE;
		$this->promo_cuba           = FALSE;
	}
  
  public function index(){
  
    echo "Will Be new version of hablax APP APi"
    
  }
  
  
}