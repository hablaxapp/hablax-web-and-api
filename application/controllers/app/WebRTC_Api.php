<?php
defined('BASEPATH') OR exit('No direct script access allowed');

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");

class WebRTC_Api extends CI_Controller {
  
  private $account;
  
  //Api Construct function
  public function __construct(){
    parent::__construct();
    
    $this->api_version          = "1.0";
		$this->app_version          = "2.0.0";
		$this->app_minimum_version  = "2.0.0";
		$this->test_minimum_version = "2.0.0";
		$this->description          = "WebRTC API for Hablax.com Hybrid app";
		$this->author               = "Ing. Gian Barboza";
    $this->result               = new stdClass();
    $this->encrypt_key          = "BkT07bCHhbVQagia7wx4CkYSxcxP7jn7";
    
    //Add the necessary models
    $this->load->model("WebRTC_Api_model","Api_model");
  }
  
	public function index()
	{
		//devolver status api
		$this->result->description          = $this->description;
		$this->result->api_version          = $this->api_version;
		$this->result->app_version          = $this->app_version;
		$this->result->app_minimum_version  = $this->app_minimum_version;
		$this->result->test_minimum_version = $this->test_minimum_version;
		$this->result->author               = $this->author;
		$this->print_json();
	}
  
  private function print_json()
	{
		header('Content-Type: application/json');
		print(json_encode($this->result));
	}


  public function get_account_data($email){
    if(!empty($this->Api_model->get_account_data($email))){
      
      $this->account = $this->Api_model->get_account_data($email);
      
      return TRUE;    
    }else{      
      return FALSE;
    }

  }

  	private function is_post()
	{
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			return TRUE;
		} else {
			#POST es necesario para ejecutar el login
			$this->set_error('post_data_required');
			return FALSE;
		}
	}

	private function set_token($token)
	{
		$this->token = $token;
	}

	private function get_token()
	{
		return $this->token;
	}

  	private function is_token_present()
	{

		if ($this->input->post('token', TRUE)) {
			return $this->is_token_valid($this->input->post('token', TRUE));
		} else {
			#Es obligatorio el token para continuar
			$this->set_error('token_required');
			return FALSE;
		}
	}

	private function is_token_valid($token)
	{
		$where = array('token' => $token);

		$query = $this->db->get_where("app_sessions", $where, 1);
		if ($query->num_rows() > 0) {
			$this->set_token($query->row()->token);
			$this->user_id   = $query->row()->account_id;
			$this->user_data = $this->Api_model->get_user_data($this->user_id);
			$this->account   = $this->user_data;
			#check if is deactivated
			if ($this->account->status == '1') {
				#broke all sessions
				$where = array('account_id' => $this->user_id);
				$this->db->delete("app_sessions", $where);
				$this->set_error('account_restricted');
				$this->print_json();
				die();
			}
			return TRUE;
		} else {
			#Token no existe
			$this->set_error('token_not_valid_or_not_found');
			return FALSE;
		}
	}

  //Function for crating sip device
  public function create_sip_device()
	{
	
	if ($this->is_post()) {

		$email = $this->input->post('email', TRUE);

		if ($this->get_account_data($email)) {
			$this->sip = new stdClass();
	        $account_sip = $this->Api_model->get_account_sip($this->account->id);
	        if (empty($account_sip)) {
	            $this->sip->host     = "198.100.164.38";
	            $this->sip->wss      = "wss://webrtc.hablax.com:7443";
	            $this->sip->username = md5($this->encrypt($this->account->id . time()));
	            $this->sip->password = md5($this->encrypt($this->account->password . time()));
	            $this->sip->uri      = $this->sip->username . "@198.100.164.38";
	            $this->sip->row_id = $this->Api_model->create_sip_device($this->account->id, $this->sip->username, $this->sip->password);
	            $this->Api_model->insert_account_sip($this->account->id, $this->sip);
	        } else {
	            $this->sip->host = $account_sip->host;
	            $this->sip->wss = $account_sip->wss;
	            $this->sip->username = $account_sip->username;
	            $this->sip->password = $account_sip->password;
	            $this->sip->uri = $account_sip->uri;
	            $this->sip->row_id = $account_sip->sip_device_id;
	        }


	        # we got the row id of sip device
	        $this->set_message('sip_created_successfull');
	        $this->result->sip = $this->sip;
		}

    }else{
      $this->set_error("get_data_error");
    }
	$this->print_json();

	}
  
  private function set_message($message_code)
	{
		$this->result->status = "success";

		switch ($message_code) {
			case 'sip_created_successfull':
				$this->result->message = "Sip created success";
				$this->result->message_code = $message_code;
				break;
        
			default:
				$this->result->message = "Unexpected error";
				$this->result->message_code = $message_code;
				break;
		}
	}
  
  private function set_error($error_code, $msg = FALSE)
	{
		$this->result->status = "error";
		switch ($error_code) {
			case 'sip_created_fail':
				$this->result->error_message = "Sip created Fail";
				$this->result->error_code = $error_code;
				break;
      case 'get_data_error':
        $this->result->error_message = "The data does not exists";
				$this->result->error_code = $error_code;
        default:
				$this->result->message = "Unexpected error";
				$this->result->message_code = $message_code;
				break;
    }
  }
  
  private function encrypt($var){
		$encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($this->encrypt_key ), $var, MCRYPT_MODE_CBC, md5(md5($this->encrypt_key ))));
		return $encrypted;
	}

	private function decrypt($var){
		$decrypted = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($this->encrypt_key ), base64_decode($var), MCRYPT_MODE_CBC, md5(md5($this->encrypt_key ))), "\0");
		return $decrypted;
	}
}